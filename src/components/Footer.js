import React from 'react';
import { Link } from "react-router-dom";
import { BsLinkedin, BsGithub, BsFacebook } from "react-icons/bs";
import newsletter from "../images/newsletter.png";

const Footer = () => {
  return (
    <>
      <footer className="py-4">
        <div className="container-xxl">
          <dv className="row align-items-center">
            <div className="col-5">
              <div className="footer-top-data d-flex gap-30 align-items-center">
                <img src={newsletter} alt="newsletter"/>
                <h2 className="mb-0 text-white">Sign Up for News Letter</h2>
              </div>
            </div>
            <div className="col-7">
              <div className="input-group">
                <input type="text" className="form-control py-1" placeholder="Your Email Address" aria-label="Your Email Address" aria-describedby="basic-addon2"/>
                <span class="input-group-text p-2" id="basic-addon2">
                  Subscribe
                </span>
              </div>
            </div>
          </dv>
        </div>
      </footer>
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-4">
              <h4 className="text-white mb-4">Contact Us</h4>
              <div>
                <address className="text-white fs-6">
                  Ingan, Hinunangan, Southern Leyte, <br/> Philippines <br/>
                  Zip Code: 6608 - 6618, <br/>
                  </address>
                  <a href="tel:+639563470390" className="mt-4 d-block text-white">+639563470390</a>
                  <a href="mailto:edajnerra328@gmail.com" className="mt-1 d-block text-white">edajnerra328@gmail.com</a>
                  <div className="social-icons d-flex align-items-center gap-30 mt-4">
                    <a href="https://www.linkedin.com/in/arren-jade-matunog-196b85241/" target="_blanc">
                      <BsLinkedin className="text-white fs-5"/>
                    </a>
                    <a href="https://github.com/renjad/" target="_blanc">
                      <BsGithub className="text-white fs-5"/>
                    </a>
                    <a href="https://www.facebook.com/arrenjadematunog" target="_blanc">
                      <BsFacebook className="text-white fs-5"/>
                    </a>
                  </div>
              </div>
            </div>
            <div className="col-3">
              <h4 className="text-white mb-4">Information</h4>
              <div className="footer-links d-flex flex-column">
                <Link to="/privacy-policy" className="text-white py-2 mb-1">Privacy Policy</Link>
                <Link to="/refund-policy" className="text-white py-2 mb-1">Refund Policy</Link>
                <Link to="/shipping-policy" className="text-white py-2 mb-1">Shipping Policy</Link>
                <Link to="/term-conditions" className="text-white py-2 mb-1">Terms &Conditions</Link>
                <Link to="/blogs" className="text-white py-2 mb-1">Blogs</Link>
              </div>
            </div>
            <div className="col-3">
              <h4 className="text-white mb-4">Account</h4>
              <div className="footer-links d-flex flex-column">
                <Link className="text-white py-2 mb-1">About Us</Link>
                <Link className="text-white py-2 mb-1">Faq</Link>
                <Link className="text-white py-2 mb-1">Contact</Link>
              </div>
            </div>
            <div className="col-2">
              <h4 className="text-white mb-4">Quick Links</h4>
              <div className="footer-links d-flex flex-column">
                <Link className="text-white py-2 mb-1">Laptops</Link>
                <Link className="text-white py-2 mb-1">Headphones</Link>
                <Link className="text-white py-2 mb-1">Tablets</Link>
                <Link className="text-white py-2 mb-1">Watch</Link>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <footer className="py-4">
        <div className="container-xxl">
          <div className="col-12">
            <p className="text-center mb-0 text-white">
              &copy; { new Date().getFullYear() }; Powered by RenJad
            </p>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer