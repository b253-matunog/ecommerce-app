import React from 'react';
import BreadCrumb from '../components/BreadCrumb';
import Meta from '../components/Meta';
import ProductCard from '../components/ProductCard';

const SingleProduct = () => {
  return (
    <>
      <Meta title={"Product Name"}/>
      <BreadCrumb title="Product Name"/>
      <div className="home-wrapper-2 main-product-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <div className='col-6'></div>
            <div className='col-6'></div>
          </div>
        </div>
      </div>
      <div className="home-wrapper-2 description-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <h4>Description</h4>
              <p className="bg-white p-3">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus non quos aliquam dolor. Consectetur maiores dolore cupiditate itaque cumque tempore.
              </p>
            </div>
          </div>
        </div>
      </div>
      <section className="home-wrapper-2 reviews-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12"></div>
          </div>
        </div>
      </section>
      <section className="home-wrapper-2 popular-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <h3 className="section-heading">
                Our Popular Products
              </h3>
            </div>
          </div>
          <div className="row">
            <ProductCard />
            <ProductCard />
            <ProductCard />
            <ProductCard />
          </div>
        </div>
      </section>
    </>
  )
}

export default SingleProduct