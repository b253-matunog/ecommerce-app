import React from 'react';
import { Link } from 'react-router-dom';
import { HiOutlineArrowLeft } from "react-icons/hi";
import BreadCrumb from '../components/BreadCrumb';
import Meta from '../components/Meta';
import Blog from "../images/blog-1.jpg";

const SingleBlog = () => {
  return (
    <>
      <Meta title={"Dynamic Blog Name"}/>
      <BreadCrumb title="Dynamic Blog Name"/>
      <div className="home-wrapper-2 blog-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="single-blog-card">
                <Link to="/blogs" className="d-flex align-items-center gap-10"><HiOutlineArrowLeft className="fs-4"/> Go back to Blogs</Link>
                <h3 className="title">
                  A Beautiful Sunday Morning renaissance 
                </h3>
                <img src={ Blog } className="img-fluid w-100 my-4" alt="blog"/>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam culpa, dolores laboriosam temporibus quo ipsa, mollitia maxime sapiente eum adipisci consequatur laudantium molestias, laborum at dolorum minima quidem quas maiores?</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default SingleBlog