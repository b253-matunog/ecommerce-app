import React from 'react';
import BreadCrumb from '../components/BreadCrumb';
import Meta from '../components/Meta';

const TermAndConditions = () => {
  return (
    <>
      <Meta title={"Term And Conditions"}/>
      <BreadCrumb title="Term And Conditions"/>
    </>
  )
}

export default TermAndConditions