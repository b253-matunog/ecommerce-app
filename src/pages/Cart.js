import React from 'react';
import BreadCrumb from '../components/BreadCrumb';
import Meta from '../components/Meta';
import watch from '../images/watch.jpg';

const Cart = () => {
  return (
    <>
      <Meta title={"Cart"}/>
      <BreadCrumb title="Cart"/>
      <section className="home-wrapper-2 cart-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="cart-header d-flex justify-content-between align-items-center py-3">
                <h4 className="cart-col-1">Product</h4>
                <h4 className="cart-col-2">Price</h4>
                <h4 className="cart-col-3">Quantity</h4>
                <h4 className="cart-col-4">Total</h4>
              </div>
              <div className="cart-data d-flex justify-content-between align-items-center py-3">
                <div className="cart-col-1 gap-15 d-flex align-items-center">
                  <div>
                    <img src={ watch } className="img-fluid" alt="Product"/>
                  </div>
                  <div className="w-75">
                    <h5 className="title">kjfdfhsdh</h5>
                    <p className="color">afd</p>
                    <p className="size">sdf</p>
                  </div>
                </div>
                <div className="cart-col-2">
                  <h5 className="price">&#8369; 5000</h5>
                </div>
                <div className="cart-col-3"></div>
                <div className="cart-col-4"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Cart