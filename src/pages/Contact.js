import React from 'react';
import BreadCrumb from '../components/BreadCrumb';
import Meta from '../components/Meta';
import { AiOutlineHome, AiOutlineMail } from "react-icons/ai";
import { BiPhoneCall, BiInfoCircle } from "react-icons/bi";

const Contact = () => {
  return (
    <>
      <Meta title={"Contact Us"}/>
      <BreadCrumb title="Contact Us"/>
      <div className="home-wrapper-2 contact-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15694.857549950946!2d125.1667027275648!3d10.444223833018556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3307022bf1a6f3a5%3A0xe72963e92ea4f306!2sIngan%2C%20Southern%20Leyte!5e0!3m2!1sen!2sph!4v1679318326591!5m2!1sen!2sph" width="600" height="450" className="border-0 w-10" allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
          </div>
          <div className="col-12 mt-5">
            <div className="contact-inner-wrapper d-flex justify-content-between">
              <div>
                <h3 className="contact-title mb-4">Contact</h3>
                <form action="" className="d-flex flex-column gap-15">
                  <div>
                    <input type="text" className="form-control" placeholder="Name"/>
                  </div>
                  <div>
                    <input type="email" className="form-control" placeholder="Email"/>
                  </div>
                  <div>
                    <input type="tel" className="form-control" placeholder="Mobile Number"/>
                  </div>
                  <div>
                    <textarea name="" id="" className="form-control w-100" color="30" rows="4" placeholder="Comments"></textarea>
                  </div>
                  <div>
                    <button className="button border-0">Submit</button>
                  </div>
                </form>
              </div>
              <div>
                <h3 className="contact-title mb-4">Get in touch with us</h3>
                <div>
                  <ul className="ps-0">
                    <li className="mb-3 d-flex align-items-center gap-15">
                      <AiOutlineHome className="fs-5"/>
                      <address className="mb-0">Ingan, Hinunangan, Southern Leyte, Philippines</address>
                    </li>
                    <li className="mb-3 d-flex align-items-center gap-15">
                      <BiPhoneCall className="fs-5"/>
                      <a href="tel:+639563470390">+639563470390</a>
                    </li>
                    <li className="mb-3 d-flex align-items-center gap-15">
                      <AiOutlineMail className="fs-5"/>
                      <a href="mailto:edajnerra328@gmail.com">edajnerra328@gmail.com</a>
                    </li>
                    <li className="mb-3 d-flex align-items-center gap-15">
                      <BiInfoCircle className="fs-5"/>
                      <p>Monday - Friday 10 AM - 8 PM</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Contact